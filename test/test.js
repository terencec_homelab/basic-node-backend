const chai = require("chai");
const chaiHttp = require("chai-http");
const { request } = require("../src/index");

const api = require("../src/index");
const should = chai.should();

chai.use(chaiHttp);

describe("GET", () => {
  describe("/hello", () => {
    it("It should respond with a hello message", (done) => {
      const request = chai
        .request(api)
        .get("/hello")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.message.should.equal("Hello !");
          done();
        });
    });
  });
  describe("/health", () => {
    it("It should respond with a health check message", (done) => {
      chai
        .request(api)
        .get("/health")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("status");
          res.body.status.should.equal("UP");
          done();
        });
    });
  });
});
