const express = require("express");
const app = express();
require("dotenv").config();

const port = process.env.PORT || 3000;

app.get("/hello", (req, res) => {
  res.status(200).json({ message: "Hello !" });
});

app.get("/health", (req, res) => {
  res.status(200).json({ status: "UP" });
});

app.listen(port, () => {
  console.log(`API listening on port ${port}`);
});

module.exports = app;
